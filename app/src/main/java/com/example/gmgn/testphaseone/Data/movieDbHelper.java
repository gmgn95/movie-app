package com.example.gmgn.testphaseone.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by gmgn on 4/10/2016.
 */
public class movieDbHelper extends SQLiteOpenHelper {
    SQLiteDatabase db;
    static final String DATABASE_NAME = "fav.db";
    static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "movies";
    public static final String COL_ID = "id";
    public static final String COL_POSTER = "poster";
    public static final String COL_BACKDROP = "backdrop";
    public static final String COL_MOV_ID = "movie_id";
    public static final String COL_MOV_NAME = "mov_name";
    public static final String COL_VOTE = "vote_avr";
    public static final String COL_DATE = "year";
    public static final String COL_OVERVIEW = "overview";


    public movieDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {


        final String MY_DATA = "CREATE TABLE " + TABLE_NAME + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COL_MOV_ID + " TEXT, " + COL_POSTER + " TEXT, "
                + COL_BACKDROP + " TEXT, " + COL_MOV_NAME + " TEXT, "
                + COL_DATE + " TEXT, " + COL_VOTE + " TEXT, "
                + COL_OVERVIEW + " TEXT" + ")";

        sqLiteDatabase.execSQL(MY_DATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
    public void Insert(String poster, String backdrop, String movId, String movName,
                       String date, String vote, String overview)
    {
         db =this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(COL_POSTER, poster);
        c.put(COL_BACKDROP, backdrop);
        c.put(COL_MOV_ID, movId);
        c.put(COL_MOV_NAME, movName);
        c.put(COL_DATE, date);
        c.put(COL_VOTE, vote);
        c.put(COL_OVERVIEW, overview);
        db.insert(TABLE_NAME, null, c); //null ==> add default value = NULL
    }
    public void deleteRow(String movId) {
        db = getWritableDatabase();
      String arr[]=new String[]{movId};
        db.delete(TABLE_NAME, COL_MOV_ID + " =?", arr);
    }

    public boolean ifExist(String movId) {
        db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{COL_MOV_ID}, COL_MOV_ID + " =?",
                new String[]{movId}, null, null, null);
        if (cursor != null && cursor.moveToNext()) {
            return true;
        } else {
            return false;
        }
    }
    public Cursor getData() {
        db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }







}