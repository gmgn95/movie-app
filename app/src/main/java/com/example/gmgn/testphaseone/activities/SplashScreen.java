package com.example.gmgn.testphaseone.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.gmgn.testphaseone.R;
import com.example.gmgn.testphaseone.splashanim.ParticleView;

public class SplashScreen extends Activity {

    ParticleView mPvGithub;
    private static int SPLASH_TIME_OUT = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        mPvGithub = (ParticleView) findViewById(R.id.pv_github);

        mPvGithub.startAnim();

        mPvGithub.setOnParticleAnimListener(new ParticleView.ParticleAnimListener() {
            @Override
            public void onAnimationEnd() {
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                SplashScreen.this.startActivity(intent);
                finish();
            }
        });


    }

}