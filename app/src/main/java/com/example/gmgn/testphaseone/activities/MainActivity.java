package com.example.gmgn.testphaseone.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.gmgn.testphaseone.Data.MovieDB;
import com.example.gmgn.testphaseone.fragments.DetailFragment;
import com.example.gmgn.testphaseone.fragments.MainFragment;
import com.example.gmgn.testphaseone.model.Movie;
import com.example.gmgn.testphaseone.adapters.MyAdapter;
import com.example.gmgn.testphaseone.R;
import com.example.gmgn.testphaseone.interfaces.myinterface;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements myinterface {
    public static MainFragment mfrg;
    private boolean mTwoPane;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Most Popular");
        if (findViewById(R.id.mydetfrag) != null) {
            mTwoPane = true;
            }

         else {
            mTwoPane = false;
        }
//        mfrg=(MainFragment)getSupportFragmentManager().findFragmentById(R.id.fragmain);

//       if (savedInstanceState == null) {
         //  mfrg=new MainFragment();
           mfrg=(MainFragment)getSupportFragmentManager().findFragmentById(R.id.fragmain);
           mfrg.setTransf(this);
          // getSupportFragmentManager().beginTransaction()
            //        .add(R.id.container,mfrg)
              //     .commit();


//        }

    }



    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.popular) {
            //TODO issue netwirk request
            mfrg.sendJsonRequest("http://api.themoviedb.org/3/movie/popular?api_key=69f8d44407d7b73a4103add4c76fccb6");
            setTitle("Most Popular");
            return true;
        }
        else if (id == R.id.fav) {
            //TODO issue netwirk request
            setTitle("Favorites");
            MovieDB db=new MovieDB(MainActivity.this);
            final ArrayList<Movie> temp1=db.getAllMovie();


          MyAdapter adapter = new MyAdapter(this,temp1,MainActivity.this, new MyAdapter.RecyclerViewClickListener() {
                @Override
                public void recyclerViewListClicked(View v, int position) {
                    transfare(temp1.get(position));

                }
            });
           // StaggeredGridLayoutManager mStaggeredHorizontalLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.HORIZONTAL); // (int spanCount, int orientation)
         //   mfrg.recyclerView.setLayoutManager(mStaggeredHorizontalLayoutManager);
            mfrg.recyclerView.setAdapter(adapter);


            return true;
        }
        else if (id == R.id.high) {

            mfrg.sendJsonRequest(" http://api.themoviedb.org/3/movie/top_rated?api_key=69f8d44407d7b73a4103add4c76fccb6");
            setTitle("Highest Rated");

            //TODO issue netwirk request
            return true;
        }


        return onOptionsItemSelected(item);

    }



    @Override
    public void transfare(Movie mymovie) {


        if(mTwoPane){
            Bundle busket=new Bundle();
            busket.putParcelable("moviekey",mymovie);

            DetailFragment detailsFragment = new  DetailFragment();
            detailsFragment.setArguments(busket);
            getSupportFragmentManager()
                    .beginTransaction().addToBackStack(null)
                    .replace(R.id.mydetfrag,detailsFragment)
                    .commit();

           /* if(Detail_Activity.mostpopular)

            {
                mfrg.sendJsonRequest("http://api.themoviedb.org/3/movie/popular?api_key=69f8d44407d7b73a4103add4c76fccb6");
                Detail_Activity.mostpopular=false;


            }
            if(Detail_Activity.toprated)
            {
                mfrg.sendJsonRequest(" http://api.themoviedb.org/3/movie/top_rated?api_key=69f8d44407d7b73a4103add4c76fccb6");
                Detail_Activity.toprated=false;


            }*/



        }
        else
        {

            Intent i=new Intent(MainActivity.this,Detail_Activity.class);
            i.putExtra("moviekey",mymovie);
            startActivity(i);


        }



    }
   /* public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }*/
}



