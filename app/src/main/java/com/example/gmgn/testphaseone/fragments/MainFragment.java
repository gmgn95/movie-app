package com.example.gmgn.testphaseone.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.gmgn.testphaseone.model.Movie;
import com.example.gmgn.testphaseone.adapters.MyAdapter;
import com.example.gmgn.testphaseone.R;
import com.example.gmgn.testphaseone.interfaces.myinterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment  {


    RequestQueue requestQueue;
   public static RecyclerView recyclerView;
    List<Movie> movies;
    MyAdapter adapter;
    myinterface transf;

    public void setTransf(myinterface transf) {
        this.transf = transf;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestQueue = Volley.newRequestQueue(getContext());
        setHasOptionsMenu(true);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.myrec);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));

        setHasOptionsMenu(true);

        return rootView;


    }

    @Override
    public void onResume() {
        super.onResume();
        sendJsonRequest("https://api.themoviedb.org/3/discover/movie?api_key=69f8d44407d7b73a4103add4c76fccb6");

    }



    public  void sendJsonRequest(String url) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, (String) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                movies = new ArrayList<>();
                try {


                    JSONArray array = jsonObject.getJSONArray("results");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.getJSONObject(i);
                        String imageUrl = object.getString("poster_path");
                        String backdrop_path = object.getString("backdrop_path");
                        String overview = object.getString("overview");
                        String release_date = object.getString("release_date");
                        String original_title = object.getString("original_title");
                        String original_language = object.getString("original_language");
                        String title = object.getString("title");
                        String vote_average = String.valueOf(object.getDouble("vote_average"));
                        String ID = object.getString("id");
                        String popularitys=String.valueOf(object.getDouble("popularity"));
                        String vote_count=String.valueOf(object.getInt("vote_count"));
                        //String tagline=object.getString("tagline");
                      //  String duration= String.valueOf(object.getInt("runtime"));
                       // movie.setRuntime(duration);
                      //  movie.setTagline(tagline);
                        Movie movie = new Movie();
                        movie.setVote_count(vote_count);
                        movie.setPopularity(popularitys);
                        movie.setImageUrl(imageUrl);
                        movie.setBackdrop_path(backdrop_path);
                        movie.setOverview(overview);
                        movie.setRelease_date(release_date);
                        movie.setOriginal_title(original_title);
                        movie.setOriginal_language(original_language);
                        movie.setTitle(title);
                        movie.setVote_average(vote_average);
                        movie.setBackdrop_path(backdrop_path);
                        movie.setId(ID);
                     //   movie.setTagline(tagline);
                        movies.add(movie);
                    }
                    adapter = new MyAdapter(getContext(), movies,getActivity(), new MyAdapter.RecyclerViewClickListener() {
                        @Override
                        public void recyclerViewListClicked(View v, int position) {
                            transf.transfare(movies.get(position));


                        }
                    });
                    recyclerView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        requestQueue.add(request);


    }

}

