package com.example.gmgn.testphaseone.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.gmgn.testphaseone.model.Movie;

import java.util.ArrayList;

public class MovieDB extends SQLiteOpenHelper {
    static final String DATABASE_NAME = "movie.db";
    private static final int DATABASE_VERSION = 2;
    public MovieDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_MOVIE_TABLE = "CREATE TABLE "+MovieContract.MovieEntry.TABLE_NAME+" ("+
                MovieContract.MovieEntry._ID + " INTEGER PRIMARY KEY, "+
                MovieContract.MovieEntry.MOVIE_TITLE+" TEXT NOT NULL, "+
                MovieContract.MovieEntry.MOVIE_RELEASE+" TEXT NOT NULL, "+
                MovieContract.MovieEntry.MOVIE_VOTE+" TEXT NOT NULL, "+
                MovieContract.MovieEntry.MOVIE_OVERVIEW+" TEXT NOT NULL, "+
                MovieContract.MovieEntry.MOVIE_POSTER+" TEXT NOT NULL, "+
                MovieContract.MovieEntry.IMAGE_URL+" TEXT NOT NULL, "+
                "UNIQUE ("+MovieContract.MovieEntry._ID+") ON CONFLICT IGNORE);";

        db.execSQL(SQL_CREATE_MOVIE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + MovieContract.MovieEntry.TABLE_NAME);
        onCreate(db);
    }

    public  void insertMovie(Movie movie){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(MovieContract.MovieEntry._ID,movie.getId());
        contentValues.put(MovieContract.MovieEntry.MOVIE_TITLE,movie.getTitle());
        contentValues.put(MovieContract.MovieEntry.MOVIE_RELEASE,movie.getRelease_date());
        contentValues.put(MovieContract.MovieEntry.MOVIE_VOTE,movie.getVote_average());
        contentValues.put(MovieContract.MovieEntry.MOVIE_OVERVIEW,movie.getOverview());
        contentValues.put(MovieContract.MovieEntry.MOVIE_POSTER,movie.getBackdrop_path());
        contentValues.put(MovieContract.MovieEntry.IMAGE_URL,movie.getImageUrl());

        db.insert(MovieContract.MovieEntry.TABLE_NAME, null, contentValues);
        db.close();
    }
    public ArrayList<Movie> getAllMovie(){
        ArrayList<Movie> movies= new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from "+ MovieContract.MovieEntry.TABLE_NAME,null);


        if(cursor.moveToFirst()) {
            do {
                Movie m= new Movie();
                m.setId(cursor.getString(0));
                m.setTitle(cursor.getString(1));
                m.setRelease_date(cursor.getString(2));
                m.setVote_average(cursor.getString(3));
                m.setOverview(cursor.getString(4));
                m.setBackdrop_path(cursor.getString(5));
                m.setImageUrl(cursor.getString(6));
                movies.add(m);
            } while(cursor.moveToNext());
        }
        db.close();

        return movies;
    }

    public void deleteMovie(String ID){
        SQLiteDatabase db= this.getWritableDatabase();
        db.delete(MovieContract.MovieEntry.TABLE_NAME, MovieContract.MovieEntry._ID+" =?",new String[]{ID});
        db.close();
    }
    public boolean ifexist(String ID2)
    {
        SQLiteDatabase db= this.getWritableDatabase();

        String Query = "Select * from " + MovieContract.MovieEntry.TABLE_NAME + " where " +  MovieContract.MovieEntry._ID + " = " + ID2;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

}