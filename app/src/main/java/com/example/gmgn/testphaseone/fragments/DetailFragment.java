package com.example.gmgn.testphaseone.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.gmgn.testphaseone.Data.MovieDB;
import com.example.gmgn.testphaseone.model.Movie;
import com.example.gmgn.testphaseone.R;
import com.example.gmgn.testphaseone.model.myreviews;
import com.example.gmgn.testphaseone.model.trailer;
import com.example.gmgn.testphaseone.adapters.trailer_adapter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class DetailFragment extends Fragment {
    String id2;
    Movie data;
    TextView title,overview,ratingView,popularityView,voteCountView,dateStatusView, durationView,languageView,taglineView,tagline,genretv;
    MovieDB db;
    boolean check = false;
    FloatingActionButton favorit;
    ImageView backdrop,poster;
    RequestQueue requestQueue;
    ArrayList<trailer>my_tris;
    CollapsingToolbarLayout collapsingToolbar;
    ArrayAdapter<trailer>adabter;
    ListView mylisttrilers;
    RecyclerView recyclerView;
    public Movie movieDetail;
    private ArrayList<String> trailerInfo = new ArrayList<>();
    Toolbar toolbar;
    private ImageView ratingsBackground, genreBackground, popBackground, langBackground;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestQueue = Volley.newRequestQueue(getContext());
        setHasOptionsMenu(true);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root= inflater.inflate(R.layout.fragment_detail, container, false);
        toolbar=(Toolbar)root.findViewById(R.id.toolbartest);
        title=(TextView)root.findViewById(R.id.title1);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
         collapsingToolbar =
                (CollapsingToolbarLayout)root.findViewById(R.id.collapsingToolbartest);

        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(getContext(),android.R.color.transparent));

        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(getContext(),android.R.color.white));
        genretv=(TextView)root.findViewById(R.id.genre);
       tagline=(TextView)root.findViewById(R.id.tagline);
        backdrop=(ImageView)root.findViewById(R.id.cover);
        poster=(ImageView)root.findViewById(R.id.posterimage);
        overview=(TextView)root.findViewById(R.id.description);
        favorit=(FloatingActionButton)root.findViewById(R.id.favor);
        ratingsBackground = (ImageView) root.findViewById(R.id.ratings_background);
        genreBackground = (ImageView) root.findViewById(R.id.genre_background);
        popBackground = (ImageView) root.findViewById(R.id.pop_background);
        langBackground = (ImageView) root.findViewById(R.id.lang_background);
        dateStatusView=(TextView)root.findViewById(R.id.date_status);
         Button review=(Button)root.findViewById(R.id.review);
        ratingView=(TextView)root.findViewById(R.id.rate);
        languageView=(TextView)root.findViewById(R.id.language);
        popularityView=(TextView)root.findViewById(R.id.popularity);
        voteCountView=(TextView)root.findViewById(R.id.vote_count);
        recyclerView=(RecyclerView)root.findViewById(R.id.recyclerViewtrailer);
recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, LinearLayout.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        Movie temp=getMovieObject();
        UpdateData(temp);
 //  int color= backdrop.getResources().getColor(R.color.material_blue_grey_900);
         popBackground.setColorFilter(Color.parseColor("#BE2F2F"), PorterDuff.Mode.MULTIPLY);
        ratingsBackground.setColorFilter(Color.parseColor("#BE2F2F"),PorterDuff.Mode.MULTIPLY);
        genreBackground.setColorFilter(Color.parseColor("#BE2F2F"),PorterDuff.Mode.MULTIPLY);
        langBackground.setColorFilter(Color.parseColor("#BE2F2F"),PorterDuff.Mode.MULTIPLY);
        int id=backdrop.getId();

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

      //  toolbar.inflateMenu(R.menu.menu_movie_detail);


     /*   toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (item.getItemId() == R.id.action_share) {
                   // String[] data = trailerInfo.get(0).split(",,");
                    //startActivity(Intent.createChooser(shareIntent(TmdbUrls.YOUTUBE_URL + data[0]), "Share Via"));
                    return true;
                }
                return true;
            }
        });*/


        /*
        Bitmap bitmap = BitmapFactory.decodeResource( getResources(),id );
        Palette.from( bitmap ).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated( Palette palette ) {
                //work with the palette here
                setViewSwatch( popBackground, palette.getVibrantSwatch() );

            }
        });*/

        my_tris = new ArrayList<trailer>();
        /*
        mylisttrilers=(ListView)root.findViewById(R.id.trailer_list);
        mylisttrilers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(my_tris.get(position).getKey())));
            }
        });
        */
        db=new MovieDB(getContext());
        checkfavorit();
        movieDetail=getArguments().getParcelable("moviekey");

        if(getArguments().getParcelable("moviekey")!=null)
        {
            UpdateData((Movie) getArguments().getParcelable("moviekey"));

        }

        favorit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(check ==false) {
                    favorit.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_like));
                    check =true;
                    db.insertMovie(data);
                    Toast.makeText(getContext(),"movie added to favorites",Toast.LENGTH_SHORT).show();

                }
                else {
                    favorit.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_like_outline));
                    check =false;
                    db.deleteMovie(id2);
                    Toast.makeText(getContext(),"movie deleted from favorites",Toast.LENGTH_SHORT).show();

                }


            }
        });

        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getActivity(),myreviews.class);
                i.putExtra("ID",id2);
                startActivity(i);
            }
        });


        return root;
    }

    public void checkfavorit(){

        if(db.ifexist(id2))
        {

            favorit.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_like));
            check= true;

        }

    }
 /*   public void sendJsonRequest(String url) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, (String) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                my_tris = new ArrayList<trailer>();
                try {
                    JSONArray array = jsonObject.getJSONArray("results");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        trailer TRI = new trailer();
                        TRI.setKey(object.getString("key"));
                        TRI.setName(object.getString("name"));
                        my_tris.add(TRI);
                    }
                  //  adabter=new ArrayAdapter<trailer>(getActivity(),R.layout.railer_list_item,R.id.trailer_name,my_tris);
                   // mylisttrilers.setAdapter(adabter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        requestQueue.add(request);


    }
*/
 private void getTrailerInfo(final String id) {
     trailerInfo.clear();
     String requestUrl = "http://api.themoviedb.org/3/movie/" + id + "/videos?" + "api_key=c6834e30cc99f62236db8a140ddd023e";

     JsonObjectRequest mTrailerRequest = new JsonObjectRequest(requestUrl, new Response.Listener<JSONObject>() {
         @Override
         public void onResponse(JSONObject response) {
             try {
                 JSONArray mTrailerArray = response.getJSONArray("results");
                 for (int i = 0; i < mTrailerArray.length(); i++) {
                     JSONObject mTrailerObject = mTrailerArray.getJSONObject(i);
                     trailerInfo.add(mTrailerObject.getString("key") + ",," + mTrailerObject.getString("name")
                             + ",," + mTrailerObject.getString("site") + ",," + mTrailerObject.getString("size"));
                 }
             } catch (JSONException e) {
                 e.printStackTrace();
             } finally {
                 // Specify Adapter
                 trailer_adapter mAdapter = new trailer_adapter(getContext(), trailerInfo);
                // Toast.makeText(getContext(),trailerInfo.get(0),Toast.LENGTH_LONG).show();
                 recyclerView.setAdapter(mAdapter);

             }

         }
     }, new Response.ErrorListener() {
         @Override
         public void onErrorResponse(VolleyError error) {
         }
     });

     requestQueue.add(mTrailerRequest);
 }

    public Movie getMovieObject(){
        if(getArguments().getParcelable("moviekey")!=null) {
            data = getArguments().getParcelable("moviekey");
        }

        return data;
    }

    public void UpdateData(Movie movie){
        data = movie;
        if(data!=null) {
            Picasso.with(getContext()).load(Uri.parse( data.getBackdrop_path())).into(backdrop);
              Picasso.with(getContext()).load(Uri.parse( data.getImageUrl())).into(poster);
              title.setText(data.getOriginal_title());
            tagline.setText(data.getTagline());
             overview.setText(data.getOverview());
            languageView.setText(data.getOriginal_language());
            voteCountView.setText(data.getVote_count()+" votes");
            popularityView.setText(data.getPopularity().substring(0,4));
          //  durationView.setText(data.getRuntime());
            dateStatusView.setText(data.getRelease_date()+" (Released)");
            ratingView.setText((data.getVote_average()));
            collapsingToolbar.setTitle(data.getOriginal_title());

            id2=data.getId();
           // Toast.makeText(getActivity(),id2,Toast.LENGTH_LONG).show();
          //  Uri build=Uri.parse("https://api.themoviedb.org/3/movie").buildUpon().appendPath(id2).appendPath("videos").appendQueryParameter("api_key","69f8d44407d7b73a4103add4c76fccb6").build();
           // sendJsonRequest(build.toString());
            getTrailerInfo(id2);

        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
         //   Toast.makeText(getContext(),"test sharing",Toast.LENGTH_LONG).show();
            String[] data = trailerInfo.get(0).split(",,");
            startActivity(Intent.createChooser(shareIntent("http://www.youtube.com/watch?v=" + data[0]), "Share Via"));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public Intent shareIntent(String data) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.movie_extra_subject));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, data);
        return sharingIntent;
    }





}