package com.example.gmgn.testphaseone.Data;

import android.provider.BaseColumns;

/**
 * Created by gmgn on 4/25/2016.
 */
public class MovieContract {
    public static final class MovieEntry implements BaseColumns {

        public static final String TABLE_NAME = "movie";
        public static final String  MOVIE_TITLE = "title";
        public static final String  MOVIE_RELEASE ="year";
        public static final String  MOVIE_POSTER = "poster";
        public static final String  MOVIE_OVERVIEW ="overview";
        public static final String  MOVIE_VOTE ="vote";
        public static final String  IMAGE_URL ="imageurl";

    }
}