package com.example.gmgn.testphaseone.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gmgn.testphaseone.R;
import com.example.gmgn.testphaseone.animation.AnimationUtils;
import com.example.gmgn.testphaseone.model.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by gmgn on 3/24/2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.Movieholder> {
    List<Movie> moviesList ;
    Context context ;
    LayoutInflater layoutInflater;
    Activity activity;
    private static RecyclerViewClickListener itemListener;
    private int previousPosition = 0;


    public MyAdapter(Context context,List<Movie> moviesList,Activity activity,RecyclerViewClickListener listener) {

        this.moviesList = moviesList;
        this.context=context;
        layoutInflater=layoutInflater.from(context);
        this.activity=activity;
     this.itemListener=listener;
    }


    @Override
    public Movieholder onCreateViewHolder(final ViewGroup parent, int viewType) {

      View row= LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_raw,parent,false);
        Movieholder holder=new Movieholder(row);
        return holder;

    }

    @Override
    public void onBindViewHolder(Movieholder holder, final int position) {

        Movie movieobj=moviesList.get(position);
      holder.name.setText(movieobj.getTitle());
        Picasso.with(context).load(Uri.parse(movieobj.getImageUrl())).into(holder.moviepic);

        if (position > previousPosition){
            AnimationUtils.animate(holder,true);
        }
        else {
            AnimationUtils.animate(holder, false);
        }
        previousPosition = position;




    }

    /*class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int itemPosition = MainFragment.recyclerView.getChildPosition(v);


        }
    }*/



    @Override
    public int getItemCount() {

        return moviesList.size();
    }
    class Movieholder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView moviepic;

        TextView name;
        public Movieholder(View itemView) {
            super(itemView);
            moviepic= (ImageView)itemView.findViewById(R.id.movieposteIMG);
            name=(TextView)itemView.findViewById(R.id.txv_row);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            itemListener.recyclerViewListClicked(view,this.getLayoutPosition());

        }
    }
    public interface RecyclerViewClickListener
    {

         void recyclerViewListClicked(View v, int position);
    }

}
