package com.example.gmgn.testphaseone.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by gmgn on 3/24/2016.
 */
public class Movie implements Parcelable {
    private  String id;
    private String imageUrl;
    private String Backdrop_path;
    private String tagline;
    private String runtime;
 private String genres;
    private String vote_count;

    public String getVote_count() {
        return vote_count;
    }

    public void setVote_count(String vote_count) {
        this.vote_count = vote_count;
    }

    private String popularity;

    public String getPopularity() {
      //  String s="";
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public Movie(){

   }
    protected Movie(Parcel in) {
        id = in.readString();
        imageUrl = in.readString();
        Backdrop_path = in.readString();
        overview = in.readString();
        release_date = in.readString();
        original_title = in.readString();
        original_language = in.readString();
        title = in.readString();
        vote_average = in.readString();
        popularity=in.readString();
        genres=in.readString();
        vote_count=in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getBackdrop_path() {
        return "http://image.tmdb.org/t/p/w780/"+Backdrop_path;
    }
    public void setBackdrop_path(String backdrop_path) {
        this.Backdrop_path = backdrop_path;
    }

    private String overview;
    private String release_date;
    private String original_title;
    private String original_language;
    private String title;
    private String vote_average;


    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVote_average() {
        return  vote_average;
    }

    public void setVote_average(String vote_average) {
        this.vote_average = vote_average;
    }


    public String getImageUrl() {
        return "http://image.tmdb.org/t/p/w342/"+imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(imageUrl);
        parcel.writeString(Backdrop_path);
        parcel.writeString(overview);
        parcel.writeString(release_date);
        parcel.writeString(original_title);
        parcel.writeString(original_language);
        parcel.writeString(title);
        parcel.writeString(vote_average);
        parcel.writeString(popularity);
        parcel.writeString(genres);
        parcel.writeString(vote_count);
    }

}
